import * as plugins from './smartbrowser.plugins.js';

import * as interfaces from './interfaces/index.js';

/**
 * SmartBrowser
 */
export class SmartBrowser {
  public headlessBrowser: plugins.smartpuppeteer.puppeteer.Browser;
  public smartpdf: plugins.smartpdf.SmartPdf;

  /**
   * start the SmartBrowser instance
   */
  public async start() {
    this.headlessBrowser = await plugins.smartpuppeteer.getEnvAwareBrowserInstance();
    this.smartpdf = new plugins.smartpdf.SmartPdf();
    await this.smartpdf.start(this.headlessBrowser);
  }

  /**
   * stop the SmartBrowser instance
   */
  public async stop() {
    await this.headlessBrowser.close();
    await this.smartpdf.stop();
  }

  /**
   * create a pdf from page
   * @param urlArg
   */
  public async pdfFromPage(urlArg: string): Promise<plugins.smartpdf.IPdf> {
    const result = await this.smartpdf.getFullWebsiteAsSinglePdf(urlArg);
    return result;
  }

  /**
   * make a screenshot from a page
   * @param urlArg
   */
  public async screenshotFromPage(urlArg: string): Promise<interfaces.IScreenShotResult> {
    const pageId = plugins.smartunique.shortId();
    const page = await this.headlessBrowser.newPage();
    await page.goto(urlArg, {
      waitUntil: 'networkidle2',
    });
    const screenshotBuffer = await page.screenshot({
      encoding: 'binary',
    }) as Buffer;
    await page.close();
    return {
      name: pageId,
      id: `${pageId}.js`,
      buffer: screenshotBuffer,
    };
  }

  /**
   * evalutes an expression on a page
   * @param urlArg
   * @param funcArg
   */
  public async evaluateOnPage<T>(urlArg: string, funcArg: () => Promise<T>) {
    const page = await this.headlessBrowser.newPage();
    await page.goto(urlArg, {
      waitUntil: 'networkidle2',
    });
    const result = await page.evaluate(funcArg);
    await page.close();
    return result;
  }
}
